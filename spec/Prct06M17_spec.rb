require "lib/Prct06M17.rb"

describe Preguntas do
 before :each do
   respuestas = ["Respuesta 1", "Respuesta 2"]
   @p1 = Preguntas.new("Pregunta 1", respuestas)
 end    

 describe "# Pregunta" do
   it "Se almacena correctamente la pregunta" do
     @p1.pregunta.should eql "Pregunta 1"
   end
 end
 
 describe "# Respuestas" do
   it "Se almacena correctamente la respuesta 1" do
     @p1.respuestas[0].should eql "Respuesta 1"
   end
   it "Se almacena correctamente la respuesta 2" do
     @p1.respuestas[1].should eql "Respuesta 2"
   end
 end

 describe "# Obtener Pregunta" do
   it "Se obtiene la pregunta 1" do
     @p1.obtenerPregunta.should eql "Pregunta 1"
   end
 end

 describe "# Obtener Respuestas" do
   it "Se obtienen todas las respuestas" do
     respuestas = ["Respuesta 1", "Respuesta 2"]
     @p1.obtenerRespuestas.should eql respuestas
   end
 end

 describe "# Imprimir Pregunta y Respuestas" do
   it "Se imprimen la pregunta 1 y todas las respuestas" do
     @p1.to_s.should eql "Pregunta 1 \n Respuesta 1\nRespuesta 2 \n"
   end
 end
end

 describe Node do
   before :each do
    @n1 = Node.new("5", nil)
    @n2 = Node.new("6", @n1)
   end

  describe "Node" do
    it "Debe existir un Nodo de la lista con sus datos y su siguiente" do
     @n2[:value].should eql "6"
     @n2[:next].should eql @n1
    end
  end
 end

 describe List do
  before :each do
   @n1 = List.new("5")
   @n2 = List.new("10")
   @n3 = List.new("15")

   respuestas1 = ["#<Xyz:0xa000208>", "nil", "0", "Ninguna de las anteriores"]
   respuestas2 = ["Cierto", "Falso"]
   respuestas3 = ["1", "bob", "HEY!", "Ninguna de las anteriores"]
   respuestas4 = ["Una instancia de la clase Class", "Una constante", "Un objeto", "Ninguna de las anteriores"]
   respuestas5 = ["Cierto", "Falso"]
   @p1 = Preguntas.new("Cual es la salida del siguiente codigo Ruby? \n class Xyz \n def pots \n @nice \n end \n end \n xyz = Xyz.new \n p xyz.pots", respuestas1)
   @p2 = Preguntas.new("La siguiente definicion de un hash en Ruby es valida: \n hash_raro = { \n [1, 2, 3] => Object.new(), \n Hash.new => :toto \n }", respuestas2)
   @p3 = Preguntas.new("Cual es la salida del siguiente codigo Ruby?", respuestas3)
   @p4 = Preguntas.new("Cual es el tipo del objeto en el siguiente codigo Ruby? \n class Objeto \n end", respuestas4)
   @p5 = Preguntas.new("Es apropiado que una clase Tablero herede de una clase Juego", respuestas5)

   @n4 = List.new(@p1)
   @n4.add ([@p2, @p3, @p4, @p5])
  end    

  describe "List" do
   it "#Se extrae el primer elemento de la lista" do
     @n1.removeFirst
     @n1.head.should eql nil
   end

   it "#Se puede insertar un elemento" do
     @n2.add ("20");
     @n2.to_s.should eql "10 -> 20"
   end

   it "#Se pueden insertar varios elementos" do
     @n2.add (["30", "40"]);
     @n2.to_s.should eql "10 -> 30 -> 40"
   end
   
   it "#Existe lista con su cabeza" do
     @n3.head[:value].should eql "15"
   end
  end

  describe "Lista de Preguntas" do
   it "# Pregunta 1" do
     @n4.head[:value].to_s.should eql "Cual es la salida del siguiente codigo Ruby? \n class Xyz \n def pots \n @nice \n end \n end \n xyz = Xyz.new \n p xyz.pots \n #<Xyz:0xa000208>\nnil\n0\nNinguna de las anteriores \n"
   end
   it "# Pregunta 2" do
     @n4.head[:next][:value].to_s.should eql "La siguiente definicion de un hash en Ruby es valida: \n hash_raro = { \n [1, 2, 3] => Object.new(), \n Hash.new => :toto \n } \n Cierto\nFalso \n"
   end
   it "# Pregunta 3" do
     @n4.head[:next][:next][:value].to_s.should eql "Cual es la salida del siguiente codigo Ruby? \n 1\nbob\nHEY!\nNinguna de las anteriores \n"
   end
   it "# Pregunta 4" do
     @n4.head[:next][:next][:next][:value].to_s.should eql "Cual es el tipo del objeto en el siguiente codigo Ruby? \n class Objeto \n end \n Una instancia de la clase Class\nUna constante\nUn objeto\nNinguna de las anteriores \n"
   end
   it "# Pregunta 5" do
     @n4.head[:next][:next][:next][:next][:value].to_s.should eql "Es apropiado que una clase Tablero herede de una clase Juego \n Cierto\nFalso \n"
   end
  end
 end